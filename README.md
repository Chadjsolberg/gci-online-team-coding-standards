# Code Style #

### Spacing and Indentation ###

Always use tabs to properly space code: 

```js
// Good
function greet(name) {
    return name;
}

// Bad
function greet(name) {
return name;
}
```

### Objects and Arrays ###
Break out objects and arrays to multiple lines: 

```
//Good
string[] names = {
    "Chad",
    "Tommy",
    "Ted",
    "Li"
}

//Bad
string[] names = {"Chad", "Tommy", "Ted", "li"}
```

### Variable Names ###
Don't abbreviate variable names. Use camelCase and write out a short but descriptive names: 
```
//Good 
var productPrice = 300;

//Bad
var pp = 300;
```

### Ternary Operators ###
Use Ternary operators where it makes sense and maintains code redability. Long nested ternary operators make it difficult to read and comprehend what is going on: 

```
//Good
direction == 1 ? dosomething () : dosomethingelse ();

//Bad
int median(int a, int b, int c) {
    return (a<b) ? (b<c) ? b : (a<c) ? c : a : (a<c) ? a : (b<c) ? c : b;
}


```

### Comments ###
Avoid self-explanatory comments. Use comments to explain complex functions or concepts. Try explaing the 'why' of the code versus the 'what' using comments:
```
//Good
/*
set age so that getTaxRate() can run before you get customer details
*/
int age = 32;

//Bad
/*
set the value of the age integer to 32
*/
int age = 32;

```

### Outputs ###
No outputs to the console.



